@extends('layout.apps')
	@section('content')

		@extends('common.error')

		<!--register form-->
		<center>
        <h3>Register</h3>
		<form class="form-horizontal" role="form" action="/register" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
		{{csrf_field()}}
		<div class="form-group">
			Fullname <br>
		    <input type="text" class="form-control" name="fullname"  style="width:250px" >
		</div>
		<div class="form-group">
			Username <br>
		    <input type="text" class="form-control" name="username" style="width:250px">
		</div>
		<div class="form-group">
			Password <br>
		    <input type="password" class="form-control" name="password"  style="width:250px" >
		</div>
		<div class="form-group">
			Confirm Password <br>
		    <input type="password" class="form-control" name="confpassword" style="width:250px" >
		</div>
		<div class="form-group">
			Email <br>
		    <input type="text" class="form-control" name="email"  style="width:250px" >
		</div>
		<div class="form-group">
			Date of Birth <br>
		    <input type="date" class="form-control" name="dob"  style="width:250px">
		</div>
		<div class="form-group">
			Gender <br>
		<select class="form-control" id="gender" style="width:250px" name="gender">
			<option value="" selected disabled>Select Gender</option>
				<option value="Male">
					Male
				</option>
				<option value="Female">
					Female
				</option>
		</select>
		</div>
		<div class="form-group">
			Phone <br>
		    <input type="text" class="form-control" name="phone"  style="width:250px" >
		</div>
		<div class="form-group">
			Address <br>
			<textarea class="form-control" rows="5" name="address"  style="width:250px"></textarea>
		</div>
		<div class="form-group">
		    <input type="submit" value="Register" class="btn btn-default">
		    <a href="{{url('/home')}}" class="btn btn-danger active" role="button">Cancel</a>
		</div>
	</form>
	</center>
	@endsection