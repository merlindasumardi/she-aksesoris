@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @elseif(session('success'))
    {{-- <br><br> --}}

	 <div class="alert alert-success">
        {{-- <br><br> --}}
        <ul><li>{{session('success')}}</li></ul>   
    </div>
@endif