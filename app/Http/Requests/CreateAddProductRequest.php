<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateAddProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'productname'=>'required',
            'category'=>'required',
            'price'=>'required|numeric',
            'stock'=>'required|numeric',
            'desc'=>'required',
            'image'=>'required',        ];
    }

     /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'productname.required' => 'Product Name is required',
            'category.required' => 'Category must be Choosen',
            'price.required' => 'Price must be filled',
            'price.numeric' => 'Price must be numeric',
            'stock.required' => 'Stock must be filled',
            'stock.numeric' => 'Stock must be numeric',
            'desc.required' => 'Description must be filled',
            'image.required' => 'Image must be filled',
        ];
    }
}
