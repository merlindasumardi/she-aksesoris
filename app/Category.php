<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['categoryname'];

    protected $primaryKey = 'id';

    /**
     * Get the products for category.
     */
    public function product()
    {
        return $this->hasMany('App\product', 'categoryid');
    }
}
