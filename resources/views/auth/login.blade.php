@extends('layout.apps')
@section('content')

@extends('common.error')

<center>
<form class="form-horizontal" role="form" action="/login" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
    {!! csrf_field() !!}

    <div class="form-group">
            Username <br>
            <input type="text" class="form-control" name="username" style="width:250px">
        </div>
        <div class="form-group">
            Password <br>
            <input type="password" class="form-control" name="password"  style="width:250px" >
        </div>

    <div class= "form-group">
        <input type="checkbox" name="remember"> Remember Me
    </div>

    <div class="form-group">
            <input type="submit" value="Login" class="btn btn-default">
            <a href="{{url('/home')}}" class="btn btn-danger active" role="button">Cancel</a>
        </div>
</form>
</center>