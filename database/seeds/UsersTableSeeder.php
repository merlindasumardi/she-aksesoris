<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrator',
            'username' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('asd123'),
            'dob' => '1990/01/01',
            'gender'=>'female',
            'phone' => '0987654321',
            'address' => 'Binus',
            'role'=> '1',
            ]);
    }
}
