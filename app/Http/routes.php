<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/home', 'homecontroller@index');
Route::get('/home', 'homecontroller@show');


//login
	Route::get('/login','logincontroller@index');
	Route::post('/login','logincontroller@store');

//logout
Route::get('auth/logout', 'Auth\AuthController@getLogout');

//register
Route::get('/register','registercontroller@index');	
Route::post('/register','registercontroller@store');	

//product
// Route::get('/product','productcontroller@index');	
Route::get('/product','productcontroller@show');
Route::get('/addproduct','productcontroller@create');
Route::post('/addproduct','productcontroller@store');
Route::get('/updateproduct/{id}', 'productcontroller@edit');
Route::post('/updateproduct/{id}','productcontroller@update');
Route::delete('/product/{id}/remove','productcontroller@destroy');

//searching
Route::post('/product','searchcontroller@mainSearch');
Route::get('/search/autocomplete', 'searchcontroller@autocomplete');
Route::get('/product/{param}', 'searchcontroller@productSearch'); 
