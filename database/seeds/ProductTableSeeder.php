<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//product 1
        DB::table('product')->insert([
        	'categoryid' => '1',
        	'productname'=> 'Kalung Hermes Pendant',
        	'price' => '90000',
        	'stock' => '10',
        	'description' => 'Best Seller',
        	'image' => 'images/noimage.png',

        	]);

        //product 2
        DB::table('product')->insert([
        	'categoryid' => '1',
        	'productname'=> 'Kalung Fendi Karlito',
        	'price' => '100000',
        	'stock' => '10',
        	'description' => 'Best Seller',
        	'image' => 'images/noimage.png',

        	]);

        //product 1
        DB::table('product')->insert([
        	'categoryid' => '2',
        	'productname'=> 'Gelang Cartier Paku',
        	'price' => '150000',
        	'stock' => '10',
        	'description' => 'Best Seller',
        	'image' => 'images/noimage.png',

        	]);

        //product 1
        DB::table('product')->insert([
        	'categoryid' => '4',
        	'productname'=> 'Bros Chanel Bunga',
        	'price' => '70000',
        	'stock' => '10',
        	'description' => 'Best Seller',
        	'image' => 'images/noimage.png',

        	]);

        //product 1
        DB::table('product')->insert([
        	'categoryid' => '4',
        	'productname'=> 'Bros Motif Burung',
        	'price' => '50000',
        	'stock' => '10',
        	'description' => 'Best Seller',
        	'image' => 'images/noimage.png',

        	]);

        //product 1
        DB::table('product')->insert([
        	'categoryid' => '5',
        	'productname'=> 'Bag Charm Karlito Tas ',
        	'price' => '110000',
        	'stock' => '10',
        	'description' => 'Tas bisa dibuka dan di isi',
        	'image' => 'images/noimage.png',

        	]);

        //product 1
        DB::table('product')->insert([
        	'categoryid' => '5',
        	'productname'=> 'Bag Charm Boneka Kelinci',
        	'price' => '130000',
        	'stock' => '10',
        	'description' => 'Bulu asli, ukuran sebesar telapak tangan',
        	'image' => 'images/noimage.png',

        	]);

        //product 1
        DB::table('product')->insert([
        	'categoryid' => '5',
        	'productname'=> 'Bag Charm Fendi Bulu Asli',
        	'price' => '180000',
        	'stock' => '10',
        	'description' => 'Bulu asli, sangat lembut',
        	'image' => 'images/noimage.png',

        	]);

        //product 1
        DB::table('product')->insert([
        	'categoryid' => '3',
        	'productname'=> 'Cincin Bvlgari Model Bulat Hitam',
        	'price' => '120000',
        	'stock' => '10',
        	'description' => 'Premium Quality, tidak gampang hitam',
        	'image' => 'images/noimage.png',

        	]);

        //product 1
        DB::table('product')->insert([
        	'categoryid' => '3',
        	'productname'=> 'Cincin LV mata V',
        	'price' => '100000',
        	'stock' => '10',
        	'description' => 'Premium Quality, bahan dari perak',
        	'image' => 'images/noimage.png',

        	]);
    }
}
