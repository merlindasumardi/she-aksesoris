@extends('layout.apps')
@section('content')

@extends('common.error')

		<!--add product form-->
		<center>
        <h3>Add New Product</h3>
		<form class="form-horizontal" role="form" action="/addproduct" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
		{{csrf_field()}}
		<div class="form-group">
			Product Name <br>
		    <input type="text" class="form-control" name="productname"  style="width:250px" >
		</div>
		<div class="form-group">
			Category <br>
		<select class="form-control" id="category" style="width:250px" name="category">
			<option value="" selected disabled>Select Category</option>
			@foreach($categories as $count => $category)
				<option value="{{$category->id}}">
					{{$category->categoryname}}
				</option>
				@endforeach
		</select>
		</div>
		
		<div class="form-group">
			Price <br>
		    <input type="text" class="form-control" name="price" style="width:250px">
		</div>
		<div class="form-group">
			Stock <br>
		    <input type="text" class="form-control" name="stock"  style="width:250px" >
		</div>
		<div class="form-group">
			Description <br>
			<textarea class="form-control" rows="5" name="desc"  style="width:250px"></textarea>
		</div>
		<div class="form-group">
			Image <br>
			<input type="file" name="image" style="width:250px">
		</div>
		<div class="form-group">
		    <input type="submit" value="Add" class="btn btn-default">
		    <a href="{{url('/product')}}" class="btn btn-danger active" role="button">Cancel</a>
		</div>
		
	</form>
	</center>
	@endsection