<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateRegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'=>'required',
            'username'=> 'required|unique:users,username',
            'password'=> 'required|min:6|alpha_num',
            'confpassword'=> 'required|same:password',
            'email'=> 'required|email|unique:users,email',
            'dob'=>'required',
            'gender'=>'required',
            'phone'=>'required|numeric',
            'address'=>'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'fullname.required' => 'Fullname is required',
            'username.required' => 'Username is required',
            'username.unique' => 'Username already exist ',
            'password.required'  => 'Password is required',
            'confpassword.required'  => 'Confirm password is required',
            'confpassword.same'=>'Confirm password must be the same with password',
            'email.required'  => 'Email is required',
            'email.unique' => 'Email already exist',
            'dob.required' => 'Date of Birth Must be Filled',
            'gender.required' => 'Gender must be Choosen',
            'phone.required' => 'Phone must be filled',
            'phone.numeric' => 'Phone must be numeric',
            'address.required' => 'Address must be filled',
        ];
    }
}
