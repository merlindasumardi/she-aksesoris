<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use Input;
use Response;
use DB;


class searchcontroller extends Controller
{
    public function mainSearch(Request $request)
    {
        try {
            $input= $request->all();

            $products = Product::where('productname', 'like', '%'.$input['search'].'%')->paginate(8);

            if($products->count() == 0) {
                
                // return redirect()->to('/product/' . $input['search']);
                return redirect()->back();
            }
            

            return view('/product', compact('products'));
            
        } catch (Exception $e) {
            return 'Keyword Not Found';
        }
    }

    public function productSearch($search)
    {
        $product = Product::where('productname', 'LIKE', '%' . $search . '%')->paginate(4);
        // return $product;
        return view('/product', compact('product'));
    }

    public function autocomplete()
    {
        $term = Input::get('term');
        
        $results = array();
        
        $queries = DB::table('product')
            ->where('productname', 'LIKE', '%'.$term.'%')->get();
        
        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->id, 'value' => $query->productname];
        }
        return Response::json($results);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
