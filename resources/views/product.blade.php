@extends('layout.apps')
@section('content')

<div class="container">
<!-- Title -->
        <div class="row text-center">
            <div class="col-lg-12">
                <h2>Products  
                @if(Auth::check())
                @if(Auth::user()->isAdmin())
                <a href="{{url('/addproduct')}}"><span class="glyphicon glyphicon-plus"></span></a>
                @endif
                @endif
                </h2>
            </div>
        </div>

<center>
    <form class="form-inline" action="{{url('/product')}}" method="POST">
  {!!csrf_field()!!}
    <input type="text" id="search" name="search" class="form-control" size="50" placeholder="Search your product">
    <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
  </form>
</center>

@if (count($products) === 0)
                <div>
                    <br>
                    <h3>Sorry, there's no products yet.</h3>
                    <h3> Have a look at others instead :)</h3>
                </div>
            @elseif (count($products) >= 1)
<!--row-->
@foreach($products as $product)
<div class="row text-center">
            
            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="{{asset($product->image)}}" alt="">
                    <div class="caption">
                        <h4>{{$product->productname}}</h4>
                        <p>Rp {{$product->price}}</p>
                        <p>{{$product->description}}</p>
                        @if(Auth::check())
                        @if(Auth::user()->isAdmin())
                        <p>
                            <a href="{{url('/updateproduct/'.$product->id)}}" class="btn btn-primary">Update</a> 
                            <form action="{{url('/product/'.$product->id.'/remove')}}" method="post" accept-charset="utf-8">
                                 {{ csrf_field() }}
                                  {{ method_field('DELETE') }}
                                {{-- <a href="{{url('/product/'.$product->id.'/remove')}}" class="btn btn-danger">Delete</a>    --}}
                                <button type="submit" onclick="if(!confirm('Sure to delete?'))return false;" class="btn btn-danger">Delete</button>                     
                            </form>
                           
                        </p>
                        @endif
                        @endif
                    </div>

                </div>
            </div>
@endforeach
        </div>
        <center>{!!$products->render()!!}</center>
        @endif
    </div>


     @section('script')
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
  $(function()
{
   $( "#search" ).autocomplete({
    source: "{{url('search/autocomplete')}}",
    minLength: 3,
    select: function(event, ui) {
      $('#search').val(ui.item.value);
    }
  });
});
  </script>