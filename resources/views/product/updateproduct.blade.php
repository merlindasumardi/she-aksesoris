@extends('layout.apps')
@section('content')
@extends('common.error')
<center>
        <h3>Update Product</h3>
		<form class="form-horizontal" role="form" action="{{url('/updateproduct/'.$product->id)}}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
		{{csrf_field()}}
		<div class="form-group">
			Product Name <br>
		    <input type="text" class="form-control" value="{{$product->productname}}" name="productname"  style="width:250px" >
		</div>
		<div class="form-group">
			Category <br>
		<select class="form-control" id="category" style="width:250px" name="category">
			<option value="" selected disabled>Select Category</option>
			@foreach($categories as $category)
				@if($product->categoryid == $category->id)
					<option value="{{$category->id}}" selected="selected">
						{{$category->categoryname}}
					</option>
				@else
					<option value="{{$category->id}}">
						{{$category->categoryname}}
					</option>
				@endif
			@endforeach	
		</select>
		</div>
		
		<div class="form-group">
			Price <br>
		    <input type="text" class="form-control" value="{{$product->price}}" name="price" style="width:250px">
		</div>
		<div class="form-group">
			Stock <br>
		    <input type="text" class="form-control" value="{{$product->stock}}" name="stock"  style="width:250px" >
		</div>
		<div class="form-group">
			Description <br>
			<textarea class="form-control" rows="5" name="desc"  style="width:250px"></textarea>
		</div>
		<div class="form-group">
			Image <br>
			<input type="file" name="image" style="width:250px">
		</div>
		<div class="form-group">
		    <input type="submit" value="Update" class="btn btn-default">
		    <a href="{{url('/product')}}" class="btn btn-danger active" role="button">Cancel</a>
		</div>
		
	</form>
	</center>

	@endsection