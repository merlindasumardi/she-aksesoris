@extends('layout.apps')
@section('content')

<!-- Page Content -->
    <div class="container">

       <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="images/kitty.jpg" alt="...">
      
    </div>
    <div class="item">
      <img src="images/city.jpg" alt="...">
    </div>
    .<div class="item">
      <img src="images/air.jpg" alt="...">
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    
  </a>
</div>

        <hr>

        <!-- Title -->
        <div class="row">
            <div class="col-lg-12">
                <h3>Newest Products</h3>
            </div>
        </div>
        <!-- /.row -->

        <!-- Page Features -->
        @foreach($products->take(4) as $count=> $product)
        <div class="row text-center">

            <div class="col-md-3 col-sm-6 hero-feature">
                <div class="thumbnail">
                    <img src="{{asset($product->image)}}" alt="">
                    <div class="caption">
                        <h4>{{$product->productname}}</h4>
                        <p>Rp {{$product->price}}</p>
                </div>
            </div>


        </div>
        @endforeach

        <div class="row text-right">
          <a href="{{url('/product')}}">See More >></a>
        </div>
        <!-- /.row -->