<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//kategori 1
        DB::table('category')->insert([
        	'categoryname' => 'Kalung',
        	]);

        //kategori 2
        DB::table('category')->insert([
        	'categoryname' => 'Gelang',
        	]);

        //kategori 3
        DB::table('category')->insert([
        	'categoryname' => 'Cincin',
        	]);

        //kategori 4
        DB::table('category')->insert([
        	'categoryname' => 'Bros',
        	]);

        //kategori 5
        DB::table('category')->insert([
        	'categoryname' => 'Bag Charm',
        	]);
    }
}
