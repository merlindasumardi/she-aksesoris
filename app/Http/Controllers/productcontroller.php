<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CreateAddProductRequest;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use Input;
use File;

class productcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('product');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = category::get();
         return view('product.addproduct', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAddProductRequest $request)
    {
         $image = rand();
        $image = md5($image);
        
        $product = [
            'productname' => Input::get('productname'),
            'categoryid' => Input::get('category'),
            'price' => Input::get('price'),
            'stock'=> Input::get('stock'),
            'description' => Input::get('desc'),
            'image' => 'images/' . $image . '.jpg'
            ];

         $image = Input::file('image');
        $image->move('images/', $image . '.jpg');

        $product = new product($product);
            // $product->category()->associate(Category::find(Input::get('category')));
            // echo $product; exit;
            $product->save();

        return redirect()->back()-> with([ 'success' => 'Add Product Success! ' ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $products = product::paginate(8);
        return view('product', compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $product = Product::where('id', $id)->firstOrFail();

        $categories = category::all();
// dd($product->categoryid);
//         exit;

        return view('product.updateproduct', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateAddProductRequest $request, $id)
    {

        //return $id;
        // $product = product::where('id', $id)->firstOrFail();
       
        // $rand_image = md5(rand());
        $rand_image = date('Ymd_His');
        $image = Input::file('image');
        $save_img = 'images/' .$rand_image . '_' . $image->getClientOriginalName();

        // dd($save_img);
        
        $product = [
            'productname' => Input::get('productname'),
            'categoryid' => Input::get('category'),
            'price' => Input::get('price'),
            'stock'=> Input::get('stock'),
            'description' => Input::get('desc'),
            'image' => $save_img
            ];

        $image->move('images/', $save_img);
        // $product->save();
        $product = product::where('id', $id)->update($product);

        return redirect()->back()->with([ 'success' => 'Product ' . Input::get('productname') . ' Updated!!' ]);    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = product::where('id', $id)->firstOrFail();
        $product->delete();

        return redirect()->back()->with([ 'success' => 'Product ' . Input::get('productname') . ' Removed!!' ]);
    }
}
