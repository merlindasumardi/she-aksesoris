<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['categoryid', 'productname', 'price', 'stock','description', 'image'];

    protected $primaryKey = 'id';

    /**
	 * Get the category that owns the product.
	 */
	public function category()
	{
	    return $this->belongsTo('App\Category');
	}
}
